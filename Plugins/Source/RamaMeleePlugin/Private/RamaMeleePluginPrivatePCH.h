// Copyright 2015 by Nathan "Rama" Iyer. All Rights Reserved.
#pragma once

#include "Engine.h"
#include "RamaMeleePluginClasses.h"

DECLARE_LOG_CATEGORY_EXTERN(RamaMeleePlugin, Log, All);
