// Copyright 2015 by Nathan "Rama" Iyer. All Rights Reserved.
#include "RamaMeleePluginPrivatePCH.h"
#include "RamaMeleeWeapon.h"
 
//////////////////////////////////////////////////////////////////////////
// URamaMeleeWeapon

URamaMeleeWeapon::URamaMeleeWeapon(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{ 
	//!Plugin version
	BodyInstance.SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);	
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	//Type
	//		You most likely will want to use a custom object channel for this via
	//			Project Settings -> Collision
	BodyInstance.SetObjectType(ECC_WorldDynamic);
	
	//Ignore all by default, using PhysX sweeps. 
	//		If want melee weapons to block each other set this component to have an appropriate Object Type
	//			and add that object to MeleeTraceObjectTypes below;
	BodyInstance.SetResponseToAllChannels(ECR_Ignore);
	BodyInstance.SetResponseToChannel(ECC_Visibility,ECR_Block);
	   
	MeleeTraceObjectTypes.Add(EObjectTypeQuery::ObjectTypeQuery3);
}      
 
void URamaMeleeWeapon::InitializeComponent()
{
	Super::InitializeComponent();
	//~~~~~~~~~~~~~~~~~~~~~
	
	SwingPrevPose.AddUninitialized(Bodies.Num());
}

void URamaMeleeWeapon::Draw()
{
	if(!IsValid()) return;
	//~~~~~~~~~~~~~
	 
	if(bHiddenInGame) return;
	//~~~~~~~~~~~~~~~~~~~
	 
	//Dedicated Server
	if(GEngine->GetNetMode(GetWorld()) == NM_DedicatedServer)
	{
		return;
	}
	 
	//Draw the Melee Comp! Handles Skeletal Physics Assets!!!!!! (my own code)
	URamaMeleeCore::DrawPrimitiveComp(this,DrawShapes_Thickness,&DamageMap);
}
	
bool URamaMeleeWeapon::MeleeSweep(FHitResult& Hit, const TArray<FTransform>& BodyPreviousPose)
{
	AActor* Actor = GetOwner();
	FBodyInstance* RootBody = GetBodyInstance();
	if(!RootBody) return false;
	
	//RamaMeleeShape.h for definition of FMeleeSweepData
	TArray<FMeleeSweepData> DamageShapesData;
	
	URamaMeleeCore::GetMeleeSweepData(
		this, 
		BodyPreviousPose, 
		DamageShapesData,
		DamageMap
	);

	//Do All Damage Shapes First!
	for(const FMeleeSweepData& EachMeleeSweep : DamageShapesData ) //Damage
	{
		if(DrawSweeps)
		{
			EachMeleeSweep.DrawStart(
				GetWorld(), 
				DrawShapes_Thickness, 
				FColor::Red, 
				DrawShapes_Duration
			); 
			EachMeleeSweep.DrawEnd(
				GetWorld(), 
				DrawShapes_Thickness, 
				FColor::Red, 
				DrawShapes_Duration
			);
		}
		  
		//Sweep as Damage
		if(URamaMeleeCore::MeleeSweep(
			GetWorld(),
			Actor,
			Hit,
			EachMeleeSweep,
			FCollisionObjectQueryParams(MeleeTraceObjectTypes)
		)){
			//BroadCast
			if(RamaMeleeWeapon_OnHit.IsBound())
			{
				RamaMeleeWeapon_OnHit.Broadcast(Hit.GetActor(), Hit.GetComponent(), Hit.ImpactPoint, Hit.ImpactNormal, Hit.BoneName, Hit);
			}
			  
			return true;
			//~~~~ Save performance, we already hit something
		}
	}
	    
	return false;
}
 
void URamaMeleeWeapon::StartSwingDamage()
{
	if(!SwingPrevPose.Num()) 
	{
		return;
	}
	
	//Clear previous data
	SwingPrevPose[0].SetLocation(FVector::ZeroVector);
	
	DoingSwingTraces = true;
}
void URamaMeleeWeapon::StopSwingDamage()
{
	DoingSwingTraces = false;
}
	
void URamaMeleeWeapon::SwingTick()
{ 
	if(!SwingPrevPose.Num())
	{
		return;
	}
	
	if(!SwingPrevPose[0].GetLocation().IsZero()) 
	{
		FHitResult Hit;
		MeleeSweep(Hit, SwingPrevPose);
		//Hit info is broadcasted inside of MeleeSweep above.
		
		//Draw Swing Lines?
		if(DrawLines)
		{
			for(int32 v = 0; v < Bodies.Num(); v++)
			{
				if(!DamageMap.ContainsBodyIndex(v))
				{
					continue;
				}
				
				FBodyInstance* EachBody = Bodies[v];
				if(!EachBody)
				{
					return;
				}
				
				if(!SwingPrevPose.IsValidIndex(v))
				{
					continue;
				}  
				 
				URamaMeleeCore::DrawLine(
					GetWorld(),
					EachBody->GetUnrealWorldTransform().GetLocation(),
					SwingPrevPose[v].GetLocation(),
					DrawShapes_Thickness,
					FColor::Red,
					DrawShapes_Duration
				);
			}
		}
	}
	
	//Store Previous Position!  
	for(int32 v = 0; v < Bodies.Num(); v++)
	{
		FBodyInstance* EachBody = Bodies[v];
		 
		if(!EachBody || !EachBody->IsValidBodyInstance() || !SwingPrevPose.IsValidIndex(v))
		{
			continue;
		}  
		SwingPrevPose[v] = EachBody->GetUnrealWorldTransform();
	}
	
}